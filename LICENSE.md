Unless specifically indicated otherwise in a file, files are licensed under the Apache 2.0 license,
as can be found in: LICENSE-apache-2.0.txt

## Components

Folders containing external components are listed below. Each component should contain its own README file with license specified for its files. The original license text is included in those source files.

```json:table
{
    "fields":[
        "Component",
        "Path",
        "License",
        "Origin",
        "Category",
        "Version",
        "Security risk"
    ],
    "items" :[
        {
            "Component": "fff",
            "Path": "",
            "License": "MIT",
            "Origin": "https://github.com/meekrosoft/fff",
            "Category": "2",
            "Version": "v1.1",
            "Security risk": "low"
        },
        {
            "Component": "Googletest",
            "Path": "",
            "License": "BSD-3-Clause",
            "Origin": "https://github.com/google/googletest",
            "Category": "2",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "project_options",
            "Path": "",
            "License": "MIT",
            "Origin": "https://github.com/cpp-best-practices/project_options",
            "Category": "2",
            "Version": "v0.21.0",
            "Security risk": "low"
        },
        {
            "Component": "gitlint",
            "Path": "",
            "License": "Apache-2.0",
            "Origin": "https://github.com/jorisroovers/gitlint",
            "Category": "2",
            "Version": "v0.17.0",
            "Security risk": "low"
        },
        {
            "Component": "crc",
            "Path": "common",
            "License": "Public domain",
            "Origin": "http://home.thep.lu.se/~bjorn/crc/",
            "Category": "1",
            "Version": "",
            "Security risk": "low"
        }
    ]
}
```
